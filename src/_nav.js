import AuthService from './components/AuthService';
const Auth = new AuthService();

var item =[];
if(Auth.getProfile().role_name === "Admin"){
  item = [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
      // badge: {
      //   variant: 'info',
      //   text: 'NEW',
      // },
    },
    {
      title: true,
      name: 'Master Data',
      wrapper: {            // optional wrapper object
        element: '',        // required valid HTML5 element tag
        attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: ''             // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: 'Universities',
      url: '/university',
      icon: 'icon-home',
    },
    {
      name: 'Colleges',
      url: '/college',
      icon: 'icon-graduation',
    },
    {
      name: 'Courses',
      url: '/course',
      icon: 'icon-book-open',
    },
    {
      name: 'Specializations',
      url: '/specialization',
      icon: 'icon-tag',
    },
    {
      name: 'Employers',
      url: '/employee',
      icon: 'icon-user',
    },  
    {
      name: 'Students',
      url: '/student',
      icon: 'icon-user',
    },
    {
      name: 'Questions',
      
      icon: 'icon-book-open',
      children: [
        {
          name: 'MCQ',
          url: 'questions',
          icon: 'icon-book-open',
        },
        {
          name: 'Written Questions',
          url: '/written-questions',
          icon: 'icon-book-open',
        },
      ],
    },
    {
      name: 'Reports',
      
      icon: 'icon-book-open',
      children: [
        {
          name: 'Student Report',
          url: '/report/student',
          icon: 'icon-book-open',
        },
        {
          name: 'Employer',
          url: '/written-questions',
          icon: 'icon-book-open',
        },
      ],
    },
    {
      name: 'Setting',
      url: '/setting',
      icon: 'icon-settings',
    }
    
  ]
}else if(Auth.getProfile().role_name === "Student"){
  item = [
    {
      name: 'Dashboard',
      url: '/Student/Dashboard',
      icon: 'icon-user',
    },
    {
      name: 'Opportunities',
      url: '/opportunities',
      icon: 'icon-star',
    },
    {
      name: 'Profile',
      url: '/Profile',
      icon: 'icon-user',
    }
    
  ]
}else if(Auth.getProfile().role_name === "Employer"){
  item = [
    {
      name: 'Profile',
      url: '/profile',
      icon: 'icon-user',
    },
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
    },{
      name: 'Vacancies',
      url: '/vacancy',
      icon: 'icon-star',
    }
   
  ]
}


export default {
  items: item,
};
